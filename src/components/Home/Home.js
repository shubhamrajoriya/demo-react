
import React, { Component } from 'react';
import './Home.css';

class Home extends Component {
  render() {
    return (
      <div id="carousel" className="carousel slide" data-ride="carousel">
        <ul className="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" className="active"></li>
            <li data-target="#carousel" data-slide-to="1" ></li>
            <li data-target="#carousel" data-slide-to="2" ></li>
        </ul>
        <div className="carousel-inner">
            <div className="carousel-item">
                <img src="./images/banner1.jpg" alt="Banner1"></img>
            </div>
            <div className="carousel-item">
                <img src="./images/banner2.jpg" alt="Banner2"></img>
            </div>
            <div className="carousel-item">
                <img src="./images/banner3.jpg" alt="Banner3"></img>
            </div>
            </div>
            <a className="carousel-control-prev" href="#carousel" data-slide="prev">
                <span className="carousel-control-prev-icon"></span>
            </a>
            <a className="carousel-control-next" href="#carousel" data-slide="next">
                <span className="carousel-control-next-icon"></span>
            </a>
      </div>
    );
  }
}

export default Home;

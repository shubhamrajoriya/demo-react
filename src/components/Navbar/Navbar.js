import React, { Component } from 'react'
import './Navbar.css';

export class Navbar extends Component {
  render() {
    return (
      <div>
      <nav className="navbar fixed-top navbar-expand-sm navbar-dark bg-primary">
    <a className="navbar-brand" href="#"><img src={require('../images/nuno.png')} className="img-fluid" />

</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse">☰</button> 
    <div className="collapse navbar-collapse " id="navbar-collapse">
        <ul className="navbar-nav ml-auto">
            <li className="nav-item active"> <a className="nav-link" href="#home">Home</a>
            </li>
            <li className="nav-item"> <a className="nav-link" href="#about">About</a>
            </li>
            <li className="nav-item"> <a className="nav-link" href="#portfolio">Portfolio</a>
            </li>
            <li className="nav-item"> <a className="nav-link" href="#service">Services</a>
            </li>
            <li className="nav-item"> <a className="nav-link" href="#price">Prices</a>
            </li>
            <li className="nav-item"> <a className="nav-link" href="#contact">Contact</a>
            </li>
        </ul>
    </div>
</nav>        
      </div>
    )
  }
}

export default Navbar
